package sus.deadmeme.amogus.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.block.entity.PonderOrbEntity;

import java.util.UUID;

public class PonderOrb extends Block implements BlockEntityProvider {
    public PonderOrb(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new PonderOrbEntity(pos, state);
    }

    // Geckolib
    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.ENTITYBLOCK_ANIMATED;
    }


    // On use of the block, tell the block entity to run the on use function to create a portal.
    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {

        if (world.isClient) return ActionResult.PASS;

        // BRO ON GOD WHY THE HELL DOES MINECRAFT DO THIS FOR EACH HAND!!
        if (player.getActiveHand() != hand) return ActionResult.PASS;


        if (!player.getStackInHand(hand).isEmpty()) return ActionResult.PASS;

        if (!state.hasBlockEntity()) return ActionResult.FAIL;

        BlockEntity entity = world.getBlockEntity(pos);
        if (!(entity instanceof PonderOrbEntity)) return ActionResult.FAIL;

        NbtCompound nbt = new NbtCompound();
        ((PonderOrbEntity) entity).writeNbt(nbt);


        // If there is already a portal, destroy it.
        entity.markDirty();
        if (nbt.contains(PonderOrbEntity.PORTAL_KEY)) {
            UUID portalUuid = nbt.getUuid(PonderOrbEntity.PORTAL_KEY);
            if (portalUuid != null) {
                return ((PonderOrbEntity) entity).destroyPortal(world, portalUuid);
            }
        }

        // TODO add sounds and particles

        return ((PonderOrbEntity) entity).summonPortal(world, pos, hit);
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        if (world.isClient) return;

        if (!state.hasBlockEntity()) return;

        BlockEntity entity = world.getBlockEntity(pos);
        if (!(entity instanceof PonderOrbEntity)) return;

        NbtCompound nbt = new NbtCompound();
        ((PonderOrbEntity) entity).writeNbt(nbt);

        entity.markDirty();
        if (nbt.contains(PonderOrbEntity.PORTAL_KEY)) {
            UUID portalUuid = nbt.getUuid(PonderOrbEntity.PORTAL_KEY);
            if (portalUuid != null) {
                ((PonderOrbEntity) entity).destroyPortal(world, portalUuid);
            }
        }

        super.onBreak(world, pos, state, player);
    }
}
