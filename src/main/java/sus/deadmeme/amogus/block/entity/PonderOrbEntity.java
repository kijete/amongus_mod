package sus.deadmeme.amogus.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import org.checkerframework.checker.units.qual.A;
import org.lwjgl.system.CallbackI;
import qouteall.imm_ptl.core.portal.Portal;
import qouteall.q_misc_util.dimension.DimId;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.registry.AmogusBlocks;

import java.util.Iterator;
import java.util.UUID;

public class PonderOrbEntity extends BlockEntity implements IAnimatable {
    private AnimationFactory factory = new AnimationFactory(this);

    private UUID tagUuid;
    public static final String UUID_KEY = "tagUuid";
    private UUID portalUuid;
    public static final String PORTAL_KEY = "portalUuid";
    private boolean empowered = false;
    public static final String EMPOWERED_KEY = "empowered";

    public PonderOrbEntity(BlockPos pos, BlockState state) {
        super(AmogusBlocks.PONDER_ORB_ENTITY, pos, state);
    }

    public ActionResult summonPortal(World world, BlockPos pos, BlockHitResult hit) {
        if (world.isClient) return ActionResult.PASS;
        if (this.tagUuid == null) return ActionResult.FAIL;

        if (hit.getSide() == Direction.UP || hit.getSide() == Direction.DOWN) return ActionResult.PASS;


        Iterator<ServerWorld> worldIterator = world.getServer().getWorlds().iterator();
        Entity entity = null;

        entity = ((ServerWorld) world).getEntity(this.tagUuid);
        while (worldIterator.hasNext()) {
            ServerWorld servWorld = worldIterator.next();
            if (servWorld.getEntity(this.tagUuid) != null) {
                entity = servWorld.getEntity(this.tagUuid);
            }
        }
        Amogus.LOGGER.info(entity.toString());
        if (entity == null) return ActionResult.FAIL;


        // Get the position a block above the entity. If it's a player, and the orb is not empowered, use their bed instead.
        BlockPos portalPos = entity.getBlockPos().up(1);
        if (entity instanceof PlayerEntity && !this.empowered) {
            portalPos = ((ServerPlayerEntity) entity).getSpawnPointPosition().up(1);
        }

        BlockPos originPos = pos.up(1);
        originPos = originPos.offset(hit.getSide().getOpposite(), 3);

        // THis is to make the distance from the orb equal in all directions.
        if (hit.getSide().getOpposite() == Direction.EAST) originPos = originPos.east(1);
        if (hit.getSide().getOpposite() == Direction.SOUTH) originPos = originPos.south(1);

        // This is to align the portal with the block.
        Vec3d originVec = new Vec3d(originPos.getX(), originPos.getY(), originPos.getZ());
        if (hit.getSide() == Direction.EAST || hit.getSide() == Direction.WEST) {
            originVec = originVec.add(0, 0, 0.5);
        } else {
            originVec = originVec.add(0.5, 0 , 0);
        }

        Vec3i horizDir = hit.getSide().rotateClockwise(Direction.Axis.Y).getVector();
        Vec3d horizVec = new Vec3d(horizDir.getX(), horizDir.getY(), horizDir.getZ());

        Portal portal = Portal.entityType.create(world);
        portal.setOriginPos(originVec);
        portal.setDestination(new Vec3d(portalPos.getX(), portalPos.getY(), portalPos.getZ()));
        portal.setDestinationDimension(RegistryKey.of(Registry.WORLD_KEY, entity.getWorld().getRegistryKey().getValue()));
        portal.setOrientationAndSize(
                new Vec3d(0, 1, 0),
                horizVec, // For now
                2,
                3
        );
        portal.world.spawnEntity(portal);
        this.portalUuid = portal.getUuid();
        saveNbt();

        return ActionResult.SUCCESS;
    }

    // Destroy the portal to prevent clogging of resources
    public ActionResult destroyPortal(World world, UUID uuid) {
        if (world.isClient) return ActionResult.PASS;

        this.portalUuid = null;
        saveNbt();

        ((ServerWorld) world).getEntity(uuid).remove(Entity.RemovalReason.DISCARDED);

        return ActionResult.SUCCESS;
    }

    public void saveNbt() {
        NbtCompound nbt = new NbtCompound();
        writeNbt(nbt);
        readNbt(nbt);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        if (this.tagUuid != null) nbt.putUuid(UUID_KEY, this.tagUuid);
        if (this.portalUuid != null) nbt.putUuid(PORTAL_KEY, this.portalUuid);
        nbt.putBoolean(EMPOWERED_KEY, this.empowered);
        super.writeNbt(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        this.tagUuid = nbt.getUuid(UUID_KEY);
        if (nbt.contains(PORTAL_KEY)) {
            this.portalUuid = nbt.getUuid(PORTAL_KEY);
        } else {
            this.portalUuid = null;
        }
        this.empowered = nbt.getBoolean(EMPOWERED_KEY);
    }

    // Geckolib stuff
    @Override
    public void registerControllers(AnimationData animationData) {
        animationData.addAnimationController(new AnimationController<PonderOrbEntity>(this, "controller", 0, this::predicate));
    }

    @Override
    public AnimationFactory getFactory() {
        return this.factory;
    }

    private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
        event.getController().transitionLengthTicks = 0;
        event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.ponder_orb.orb", true));
        return PlayState.CONTINUE;
    }
}
