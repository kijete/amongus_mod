package sus.deadmeme.amogus.gecko.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.processor.IBone;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.item.Plushie;

public class PlushieItemModel extends AnimatedGeoModel<Plushie> {
    @Override
    public Identifier getModelLocation(Plushie object) {
        return new Identifier(Amogus.MOD_ID, "geo/plushie.geo.json");
    }

    @Override
    public Identifier getTextureLocation(Plushie object) {
        return new Identifier(Amogus.MOD_ID, "textures/item/plushie.png");
    }

    @Override
    public Identifier getAnimationFileLocation(Plushie animatable) {
        return new Identifier(Amogus.MOD_ID, "animations/plushie.animation.json");
    }

    @Override
    public void setLivingAnimations(Plushie entity, Integer Id, AnimationEvent event) {
        super.setLivingAnimations(entity, Id, event);
        IBone bone = this.getAnimationProcessor().getBone("bone");
        if (bone == null) return;
        bone.setScaleX(0.75f);
        bone.setScaleY(0.75f);
        bone.setScaleZ(0.75f);
    }
}
