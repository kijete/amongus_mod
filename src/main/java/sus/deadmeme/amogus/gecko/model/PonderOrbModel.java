package sus.deadmeme.amogus.gecko.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.block.entity.PonderOrbEntity;

public class PonderOrbModel extends AnimatedGeoModel<PonderOrbEntity> {
    @Override
    public Identifier getModelLocation(PonderOrbEntity object) {
        return new Identifier(Amogus.MOD_ID, "geo/ponder_orb.geo.json");
    }

    @Override
    public Identifier getTextureLocation(PonderOrbEntity object) {
        return new Identifier(Amogus.MOD_ID, "textures/block/ponder_orb.png");
    }

    @Override
    public Identifier getAnimationFileLocation(PonderOrbEntity animatable) {
        return new Identifier(Amogus.MOD_ID, "animations/ponder_orb.animation.json");
    }
}
