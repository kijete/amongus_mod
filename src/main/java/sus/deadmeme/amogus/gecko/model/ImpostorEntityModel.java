package sus.deadmeme.amogus.gecko.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.entity.ImpostorEntity;

public class ImpostorEntityModel extends AnimatedGeoModel<ImpostorEntity> {
    @Override
    public Identifier getModelLocation(ImpostorEntity object) {
        return new Identifier(Amogus.MOD_ID, "geo/impostor.geo.json");
    }

    @Override
    public Identifier getTextureLocation(ImpostorEntity object) {
        return new Identifier(Amogus.MOD_ID, "textures/entity/"+ object.getColorName() + "_impostor.png");
    }

    @Override
    public Identifier getAnimationFileLocation(ImpostorEntity animatable) {
        return new Identifier(Amogus.MOD_ID, "animations/impostor.animation.json");
    }
}
