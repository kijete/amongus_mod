package sus.deadmeme.amogus.gecko.renderer;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.renderers.geo.GeoBlockRenderer;
import sus.deadmeme.amogus.block.entity.PonderOrbEntity;
import sus.deadmeme.amogus.gecko.model.PonderOrbModel;

public class PonderOrbRenderer extends GeoBlockRenderer<PonderOrbEntity> {
    public PonderOrbRenderer() {
        super(new PonderOrbModel());
    }

    @Override
    public RenderLayer getRenderType(PonderOrbEntity animatable, float partialTicks, MatrixStack stack,
                                     VertexConsumerProvider renderTypeBuffer, VertexConsumer vertexBuilder, int packedLightIn,
                                     Identifier textureLocation) {
        return RenderLayer.getEntityTranslucent(getTextureLocation(animatable));
    }

}
