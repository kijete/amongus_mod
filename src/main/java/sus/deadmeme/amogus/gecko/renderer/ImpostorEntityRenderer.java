package sus.deadmeme.amogus.gecko.renderer;

import net.minecraft.client.render.entity.EntityRendererFactory;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;
import sus.deadmeme.amogus.entity.ImpostorEntity;
import sus.deadmeme.amogus.gecko.model.ImpostorEntityModel;

public class ImpostorEntityRenderer extends GeoEntityRenderer<ImpostorEntity> {
    public ImpostorEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new ImpostorEntityModel());
    }
}
