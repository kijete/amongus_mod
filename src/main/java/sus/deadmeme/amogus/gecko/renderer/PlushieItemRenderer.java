package sus.deadmeme.amogus.gecko.renderer;

import software.bernie.geckolib3.renderers.geo.GeoItemRenderer;
import sus.deadmeme.amogus.gecko.model.PlushieItemModel;
import sus.deadmeme.amogus.item.Plushie;

public class PlushieItemRenderer extends GeoItemRenderer<Plushie> {
    public PlushieItemRenderer() {
        super(new PlushieItemModel());
    }
}
