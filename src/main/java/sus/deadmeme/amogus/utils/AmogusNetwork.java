package sus.deadmeme.amogus.utils;

import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.abilities.VentAbility;

public class AmogusNetwork {
    public static final Identifier VENT_PACKET_ID = new Identifier(Amogus.MOD_ID, "vent");

    public static void init() {
        ServerPlayNetworking.registerGlobalReceiver(VENT_PACKET_ID,
                (MinecraftServer server,
                ServerPlayerEntity player,
                ServerPlayNetworkHandler handler,
                PacketByteBuf buf,
                PacketSender responseSender)
                        -> VentAbility.receiveVent(server, player, handler, buf, responseSender));
    }


}
