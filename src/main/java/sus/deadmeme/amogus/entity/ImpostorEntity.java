package sus.deadmeme.amogus.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.ai.pathing.PathNodeType;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.mob.GhastEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.HorseBaseEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.DyeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import sus.deadmeme.amogus.registry.AmogusEntities;
import sus.deadmeme.amogus.registry.AmogusSounds;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class ImpostorEntity extends TameableEntity implements IAnimatable {
    // GeckoLib stuff
    private AnimationFactory factory = new AnimationFactory(this);

    // String dictionary for getting texture name from color.
    private static final Map<Integer, String> colorMap = Map.of (
            // TODO add textures and abilites for every color
            DyeColor.RED.getId(), "red",
            DyeColor.BLUE.getId(), "blue",
            DyeColor.PURPLE.getId(), "purple",
            DyeColor.WHITE.getId(), "white",
            DyeColor.BLACK.getId(), "black",
            DyeColor.GREEN.getId(), "green",
            DyeColor.YELLOW.getId(), "yellow",
            DyeColor.CYAN.getId(), "cyan"
    );

    // Random entity statics created here
    private static final TrackedData<Integer> COLOR = DataTracker.registerData(ImpostorEntity.class, TrackedDataHandlerRegistry.INTEGER);
    private static final float WILD_MAX_HEALTH = 10.0f;
    private static final float TAMED_MAX_HEALTH = 40.0f;

    public ImpostorEntity(EntityType<? extends TameableEntity> entityType, World world) {
        super(entityType, world);
        this.ignoreCameraFrustum = true;
        this.setTamed(false);
        this.setPathfindingPenalty(PathNodeType.POWDER_SNOW, -1.0f);
        this.setPathfindingPenalty(PathNodeType.DANGER_POWDER_SNOW, -1.0f);
    }

    public static DefaultAttributeContainer.Builder createImpostorAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.2f)
                .add(EntityAttributes.GENERIC_MAX_HEALTH, WILD_MAX_HEALTH)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 6.0);
    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(1, new SwimGoal(this));
        this.goalSelector.add(1, new EscapeDangerGoal(this, 2));
        this.goalSelector.add(2, new SitGoal(this));
        this.goalSelector.add(3, new PounceAtTargetGoal(this, 0.5f));
        this.goalSelector.add(4, new MeleeAttackGoal(this, 2.5, true));
        this.goalSelector.add(5, new FollowOwnerGoal(this, 2.0, 5.0f, 1.0f, false));
        this.goalSelector.add(6, new AnimalMateGoal(this, 1.0));
        this.goalSelector.add(7, new WanderAroundFarGoal(this, 1.0));

        this.targetSelector.add(1, new TrackOwnerAttackerGoal(this));
        this.targetSelector.add(2, new AttackWithOwnerGoal(this));
    }

    @Override
    public void tick() {
        super.tick();

        // Apply the White Amogus levitation randomly
        if(this.getColor() == DyeColor.WHITE) {
            if (!world.isClient()) {
                Random rand = world.getRandom();
                if (this.age % (20 * 20) == 1 && rand.nextInt(10) <= 1) {
                    this.addStatusEffect(new StatusEffectInstance(
                            StatusEffects.LEVITATION,
                            (20 * rand.nextInt(1, 10)),
                            rand.nextInt(-1, 0)));
                }

                // Apply the effect if above a fluid or the void
                BlockPos pos = this.getBlockPos().down();
                while (world.getBlockState(pos).isAir()) {
                    pos = pos.down();

                    if (!world.getBlockState(pos).getFluidState().isEmpty()
                    || pos.getY() <= world.getBottomY()) {
                        this.addStatusEffect(new StatusEffectInstance(
                                StatusEffects.LEVITATION,
                                20,
                                -1));
                    }

                }

            }
        }
    }

    @Override
    public boolean damage(DamageSource source, float amount) {
        // White Amogus no fall damge
        if (source == DamageSource.FALL && this.getColor() == DyeColor.WHITE) return false;
        return super.damage(source, amount);
    }

    @Override
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        ItemStack itemStack = player.getStackInHand(hand);
        Item item = itemStack.getItem();
        if (this.world.isClient) {
            boolean bl = this.isOwner(player) || this.isTamed() || itemStack.isOf(Items.DIAMOND) && !this.isTamed();
            return bl ? ActionResult.CONSUME : ActionResult.PASS;
        }
        if (player.isCreative() && item instanceof DyeItem) {
            if (colorMap.get(((DyeItem) item).getColor().getId()) != null) {
                this.setColor(((DyeItem) item).getColor());
                return ActionResult.SUCCESS;
            }
        }
        if (this.isTamed()) {
            if (this.isBreedingItem(itemStack) && this.getHealth() < this.getMaxHealth()) {
                if (!player.getAbilities().creativeMode) {
                    itemStack.decrement(1);
                }
                this.heal(3);
                this.emitGameEvent(GameEvent.MOB_INTERACT, this.getCameraBlockPos());
                return ActionResult.SUCCESS;
            }
            ActionResult actionResult = super.interactMob(player, hand);
            if (actionResult.isAccepted() && !this.isBaby() || !this.isOwner(player)) return actionResult;
            this.setSitting(!this.isSitting());
            this.jumping = false;
            this.navigation.stop();
            this.setTarget(null);
            return ActionResult.SUCCESS;
        }
        if (!itemStack.isOf(Items.DIAMOND)) return super.interactMob(player, hand);
        if (!player.getAbilities().creativeMode) {
            itemStack.decrement(1);
        }
        if (this.random.nextInt(3) == 0) {
            this.setOwner(player);
            this.navigation.stop();
            this.setTarget(null);
            this.setSitting(true);
            this.world.sendEntityStatus(this, (byte)7);
            return ActionResult.SUCCESS;
        } else {
            this.world.sendEntityStatus(this, (byte)6);
        }
        return ActionResult.SUCCESS;
    }

    @Override
    public void setTamed(boolean tamed) {
        super.setTamed(tamed);
        if (tamed) {
            this.getAttributeInstance(EntityAttributes.GENERIC_MAX_HEALTH).setBaseValue(TAMED_MAX_HEALTH);
            this.setHealth(20.0f);
        } else {
            this.getAttributeInstance(EntityAttributes.GENERIC_MAX_HEALTH).setBaseValue(WILD_MAX_HEALTH);
        }
    }

    @Override
    public boolean canAttackWithOwner(LivingEntity target, LivingEntity owner) {
        if (target instanceof GhastEntity) return false;
        if (target instanceof PlayerEntity && owner instanceof PlayerEntity && !((PlayerEntity)owner).shouldDamagePlayer((PlayerEntity)target)) {
            return false;
        }
        if (target instanceof HorseBaseEntity && ((HorseBaseEntity)target).isTame()) {
            return false;
        }
        if (target instanceof TameableEntity) {
            TameableEntity tameableEntity = (TameableEntity) target;
            return !tameableEntity.isTamed() || !tameableEntity.isOwner(owner);
        }
        return true;
    }

    @Override
    public ImpostorEntity createChild(ServerWorld world, PassiveEntity entity) {
        ImpostorEntity impostorEntity = AmogusEntities.IMPOSTOR.create(world);
        UUID uUID = this.getOwnerUuid();
        if (uUID != null) {
            impostorEntity.setTamed(true);
            impostorEntity.setOwnerUuid(uUID);
        }
        return impostorEntity;
    }

    @Override
    public boolean canBreedWith(AnimalEntity other) {
        if (other == this) {
            return false;
        }
        if (!this.isTamed()) {
            return false;
        }
        if (!(other instanceof ImpostorEntity)) {
            return false;
        }
        ImpostorEntity impostorEntity = (ImpostorEntity)other;
        if (!impostorEntity.isTamed()) {
            return false;
        }
        return this.isInLove() && impostorEntity.isInLove();
    }

    @Override
    public boolean isBreedingItem(ItemStack stack) {
        Item item = stack.getItem();
        return item == Items.DIAMOND;
    }

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(COLOR, DyeColor.RED.getId());
    }

    @Override
    public void writeCustomDataToNbt(NbtCompound nbt) {
        super.writeCustomDataToNbt(nbt);
        nbt.putByte("Color", (byte)this.getColor().getId());
    }

    @Override
    public void readCustomDataFromNbt(NbtCompound nbt) {
        super.readCustomDataFromNbt(nbt);
        if (nbt.contains("Color", 99)) {
            this.setColor(DyeColor.byId(nbt.getInt("Color")));
        }
    }

    public void setColor(DyeColor color) {
        this.dataTracker.set(COLOR, color.getId());
    }

    public DyeColor getColor() {
        return DyeColor.byId(this.dataTracker.get(COLOR));
    }

    public String getColorName() {
        return colorMap.get(this.getColor().getId());
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return AmogusSounds.SOUND_AMONG_US_HIT_EVENT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        if (this.random.nextInt(10) < 3) return AmogusSounds.SOUND_AMONG_US_KILL_EVENT;
        return AmogusSounds.SOUND_REPORT_EVENT;
    }

    // GeckoLib needed stuff
    private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
        if (!(event.getLimbSwingAmount() > -0.15F && event.getLimbSwingAmount() < 0.15F)) {
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.impostor.walk", true));
            return PlayState.CONTINUE;
        } else if (this.isInSittingPose()) {
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.impostor.sit", true));
            return PlayState.CONTINUE;
        }
        return PlayState.STOP;

    }

    @Override
    public void registerControllers(AnimationData animationData) {
        animationData.addAnimationController(new AnimationController(
                this,
                "controller",
                20,
                this::predicate));
    }

    @Override
    public AnimationFactory getFactory() {
        return factory;
    }
}
