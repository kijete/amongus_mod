package sus.deadmeme.amogus.item;

import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.block.entity.PonderOrbEntity;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class Tag extends Item {

    public Tag(Settings settings) {
        super(settings);
    }

    // Just put the UUID as the tooltip.
    @Override
    public void appendTooltip(ItemStack itemStack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
        NbtCompound nbt = itemStack.getOrCreateNbt();

        if (!nbt.contains(PonderOrbEntity.UUID_KEY)) return;
        UUID uuid = nbt.getUuid(PonderOrbEntity.UUID_KEY);
        if (uuid == null) return;
        
        tooltip.add(new LiteralText(uuid.toString()).formatted(Formatting.RED, Formatting.UNDERLINE));
    }

    // If no UUID present already, steal from the entity it is used on.
    @Override
    public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
        if (user.getWorld().isClient) return ActionResult.PASS;
        NbtCompound nbt = stack.getOrCreateNbt();
        UUID uuid = null;
        if (nbt.contains(PonderOrbEntity.UUID_KEY)) {
            uuid = nbt.getUuid(PonderOrbEntity.UUID_KEY);
        }
        if (uuid != null) return ActionResult.PASS;

        nbt.putUuid(PonderOrbEntity.UUID_KEY, entity.getUuid());
        stack.setNbt(nbt);
        user.setStackInHand(hand, stack);
        return ActionResult.SUCCESS;
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        if (context.getWorld().isClient) return ActionResult.PASS;
        BlockEntity block = context.getWorld().getBlockEntity(context.getBlockPos());

        // If used on a Ponder Orb tile entity, give it the UUID.
        if((block instanceof PonderOrbEntity)) {

            NbtCompound itemNbt = context.getStack().getOrCreateNbt();

            UUID itemUuid = null;
            if (itemNbt.contains(PonderOrbEntity.UUID_KEY)) {
                itemUuid = itemNbt.getUuid(PonderOrbEntity.UUID_KEY);
            }
            if (itemUuid == null) return ActionResult.PASS;


            NbtCompound nbt = new NbtCompound();
            ((PonderOrbEntity) block).writeNbt(nbt);

            if (nbt.contains(PonderOrbEntity.PORTAL_KEY)) {
                ((PonderOrbEntity) block).destroyPortal(context.getWorld(), nbt.getUuid(PonderOrbEntity.PORTAL_KEY));
            }

            nbt.putUuid(PonderOrbEntity.UUID_KEY, itemUuid);

            block.readNbt(nbt);
            block.markDirty();

            return ActionResult.SUCCESS;

        }

        return ActionResult.PASS;
    }
}
