package sus.deadmeme.amogus.item;

import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import software.bernie.geckolib3.core.AnimationState;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import software.bernie.geckolib3.network.GeckoLibNetwork;
import software.bernie.geckolib3.network.ISyncable;
import software.bernie.geckolib3.util.GeckoLibUtil;

import java.util.List;

public class Plushie extends Item implements IAnimatable, ISyncable {
    public static final int ANIM_TRAP = 0;
    public static final String FILLED_KEY = "filled";
    public static final String ENTITY_KEY = EntityType.ENTITY_TAG_KEY;
    public static final String TYPE_KEY = "entityTypeId";

    public AnimationFactory factory = new AnimationFactory(this);
    public Plushie(Settings settings) {
        super(settings);
        GeckoLibNetwork.registerSyncable(this);
    }

    @Override
    public void appendTooltip(ItemStack itemStack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
        String typeString = itemStack.getOrCreateNbt().getString(TYPE_KEY);
        if (typeString.equals("")) {
            tooltip.add(new TranslatableText("item.amogus.plushie_tooltip_empty"));
            return;
        }

        Text entityName = Registry.ENTITY_TYPE.get(new Identifier(typeString)).getName();

        tooltip.add(new TranslatableText("item.amogus.plushie_tooltip", entityName.getString()));
    }

    // Trap entity in plushie if applicable
    @Override
    public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
        World world = user.getWorld();
        if (world.isClient) return ActionResult.PASS;

        // Just in case...
        if ( entity instanceof PlayerEntity) return super.useOnEntity(stack, user, entity, hand);

        // Get nbt data. Create if none
        NbtCompound nbt = stack.getOrCreateNbt();
        NbtCompound entityNbt = stack.getOrCreateSubNbt(ENTITY_KEY);

        boolean filled;
        if (!nbt.contains(FILLED_KEY)) {
            nbt.putBoolean(FILLED_KEY, false);
        }
        filled = nbt.getBoolean(FILLED_KEY);


        // If the plush is empty, grab the entity and add its data. Otherwise, do nothing.
        if (!filled) {
            // Remember to animate it...
            final int id = GeckoLibUtil.guaranteeIDForStack(stack, (ServerWorld) world);
            GeckoLibNetwork.syncAnimation(user, this, id, ANIM_TRAP);
            for (PlayerEntity otherPlayer : PlayerLookup.tracking(user)) {
                GeckoLibNetwork.syncAnimation(otherPlayer, this, id, ANIM_TRAP);
            }

            // Mark item as filled and give it the UUID of the entity (for tagging purposes);
            nbt.putBoolean(FILLED_KEY, true);
            nbt.putString(TYPE_KEY, Registry.ENTITY_TYPE.getId(entity.getType()).toString());

            // Get entity NBT and write as subNBT
            entity.writeCustomDataToNbt(entityNbt);
            stack.setSubNbt(ENTITY_KEY, entityNbt);
            // Change NBT
            stack.setNbt(nbt);
            // Give the player the new changed item
            user.setStackInHand(hand, stack);
            // Remove the entity
            entity.remove(Entity.RemovalReason.DISCARDED);

            return ActionResult.SUCCESS;
        }
        return super.useOnEntity(stack, user, entity, hand);
    }

    // Re-summon the entity if applicable
    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        if (world.isClient) return ActionResult.PASS;

        ItemStack stack = context.getStack();

        // Get nbt data. Create if none
        NbtCompound nbt = stack.getOrCreateNbt();

        boolean filled;
        if (!nbt.contains(FILLED_KEY)) {
            nbt.putBoolean(FILLED_KEY, false);
        }
        filled = nbt.getBoolean(FILLED_KEY);

        if (!filled) return ActionResult.FAIL;

        NbtCompound eNbt = stack.getSubNbt(ENTITY_KEY);
        String typeString = nbt.getString(TYPE_KEY);
        EntityType<?> eType = Registry.ENTITY_TYPE.get(new Identifier(typeString));
        if (eNbt == null || typeString.equals("")) return ActionResult.FAIL;

        eType.spawn((ServerWorld) world, nbt, null, null, context.getBlockPos().up(1), SpawnReason.MOB_SUMMONED, false, false);

        stack.removeSubNbt(ENTITY_KEY);
        nbt.remove(FILLED_KEY);
        nbt.remove(TYPE_KEY);

        stack.setNbt(nbt);
        context.getPlayer().setStackInHand(context.getHand(), stack);

        return ActionResult.SUCCESS;
    }

    // GeckoLib stuff
    @Override
    public void registerControllers(AnimationData data) {
        data.addAnimationController(new AnimationController(this, "controller", 1, this::predicate));
    }

    private <P extends Item & IAnimatable> PlayState predicate(AnimationEvent<P> event) {
        return PlayState.CONTINUE;
    }

    @Override
    public AnimationFactory getFactory() {
        return this.factory;
    }

    @Override
    public void onAnimationSync(int id, int state) {
        if (state == ANIM_TRAP) {
            // Always use GeckoLibUtil to get AnimationControllers when you don't have
            // access to an AnimationEvent
            @SuppressWarnings("rawtypes")
            final AnimationController controller = GeckoLibUtil.getControllerForID(this.factory, id, "controller");

            if (controller.getAnimationState() == AnimationState.Stopped) {
                // If you don't do this, the animation will only play once because the
                // animation will be cached.
                controller.markNeedsReload();
                // Animate
                controller.setAnimation(new AnimationBuilder().addAnimation("animation.plushie.trap", false));
            }
        }
    }
}
