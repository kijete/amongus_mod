package sus.deadmeme.amogus.item;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.world.World;
import sus.deadmeme.amogus.registry.AmogusMisc;
import sus.deadmeme.amogus.registry.AmogusSounds;
import java.util.Random;

public class AmongItem extends Item {
    public AmongItem(Settings settings) {
        super(settings);
    }

    @Override
    public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {

        if (!world.isClient) {
            // Have a small chance to lower the pitch of the AMOGUS sound.
            float pitch = 1f;
            Random rand = new Random();
            if (rand.nextInt(1000) == 1) pitch = 0.2f;

            // 90% chance to AMOGUS, 10% chance to AMONG US!
            if (rand.nextInt(100) < 90)
                world.playSound(null, user.getBlockPos(), AmogusSounds.SOUND_AMOGUS_EVENT, SoundCategory.PLAYERS, 1f, pitch);
            else
                world.playSound(null, user.getBlockPos(), AmogusSounds.SOUND_AMONG_US_YELL_EVENT, SoundCategory.PLAYERS, 1f, 1f);

            // Give them SUS_POWER
            user.addStatusEffect(new StatusEffectInstance(AmogusMisc.SUS_POWER, 3600, 0));
        }

        // Trigger vanilla eating effects as well.
        if (this.isFood()) return user.eatFood(world, stack);
        return stack;
    }
}
