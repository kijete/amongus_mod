package sus.deadmeme.amogus;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.glfw.GLFW;
import software.bernie.geckolib3.renderers.geo.GeoItemRenderer;
import sus.deadmeme.amogus.abilities.VentAbility;
import sus.deadmeme.amogus.registry.AmogusBlocks;
import sus.deadmeme.amogus.registry.AmogusEntities;
import sus.deadmeme.amogus.registry.AmogusItems;
import sus.deadmeme.amogus.registry.AmogusMisc;
import sus.deadmeme.amogus.utils.AmogusNetwork;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

public class AmogusClient implements ClientModInitializer {
    private static final KeyBinding ventKeybind = KeyBindingHelper.registerKeyBinding(new KeyBinding(
            "key.amogus.vent",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_V,
            "category.amogus.sus_power"
    ));

    @Override
    public void onInitializeClient() {

        ClientTickEvents.END_CLIENT_TICK.register(AmogusClient::ventKeyHandler);

        Amogus.LOGGER.info("Keybinds registered");

        AmogusMisc.clientInit();
        AmogusEntities.clientInit();
        AmogusItems.clientInit();
        AmogusBlocks.clientInit();
    }


    // Keybind handlers will be here for convenience’s sake
    private static void ventKeyHandler(MinecraftClient client) {
        // If the player has SUS_POWER, let them vent.
        if (client.player == null) return;

        Iterator<StatusEffectInstance> statusEffects = client.player.getStatusEffects().iterator();
        boolean hasPower = false;
        boolean hasCooldown = false;

        // Does player have SUS_POWER?
        while (statusEffects.hasNext()) {
            hasPower = (statusEffects.next().getEffectType() == AmogusMisc.SUS_POWER);
            if (hasPower) break;
        }

        // Reset iterator of effects.
        statusEffects = client.player.getStatusEffects().iterator();

        // Does player have SUS_COOLDOWN?
        while (statusEffects.hasNext()) {
            hasCooldown = (statusEffects.next().getEffectType() == AmogusMisc.SUS_COOLDOWN);
            if (hasCooldown) break;
        }

        if (ventKeybind.wasPressed() &&
                hasPower &&
                !hasCooldown)
            VentAbility.sendVentPacket(client);
    }
}
