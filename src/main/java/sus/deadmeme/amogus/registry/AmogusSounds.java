package sus.deadmeme.amogus.registry;

import net.minecraft.client.sound.Sound;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.lwjgl.system.CallbackI;
import sus.deadmeme.amogus.Amogus;

// This class creates, and through init() registers, the sounds of the mod.
public class AmogusSounds {

    public static final Identifier SOUND_AMOGUS_ID = new Identifier(Amogus.MOD_ID, "amogus");
    public static SoundEvent SOUND_AMOGUS_EVENT = new SoundEvent(SOUND_AMOGUS_ID);

    public static final Identifier SOUND_AMONG_US_YELL_ID = new Identifier(Amogus.MOD_ID,"among_us_yell");
    public static SoundEvent SOUND_AMONG_US_YELL_EVENT = new SoundEvent(SOUND_AMONG_US_YELL_ID);

    public static final Identifier SOUND_DRIP_ID = new Identifier(Amogus.MOD_ID, "drip");
    public static SoundEvent SOUND_DRIP_EVENT = new SoundEvent(SOUND_DRIP_ID);

    public static final Identifier SOUND_AMONG_US_HIT_ID = new Identifier(Amogus.MOD_ID, "among_us_hit");
    public static SoundEvent SOUND_AMONG_US_HIT_EVENT = new SoundEvent(SOUND_AMONG_US_HIT_ID);

    public static final Identifier SOUND_AMONG_US_KILL_ID = new Identifier(Amogus.MOD_ID, "among_us_kill");
    public static SoundEvent SOUND_AMONG_US_KILL_EVENT = new SoundEvent(SOUND_AMONG_US_KILL_ID);

    public static final Identifier SOUND_REPORT_ID = new Identifier(Amogus.MOD_ID, "report_sound");
    public static SoundEvent SOUND_REPORT_EVENT = new SoundEvent(SOUND_REPORT_ID);

    public static final Identifier SOUND_AMONG_US_WALK_ID = new Identifier(Amogus.MOD_ID, "among_us_walk");
    public static SoundEvent SOUND_AMONG_US_WALK_EVENT = new SoundEvent(SOUND_AMONG_US_WALK_ID);

    public static void init() {
        Registry.register(Registry.SOUND_EVENT, SOUND_AMOGUS_ID, SOUND_AMOGUS_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_AMONG_US_YELL_ID, SOUND_AMONG_US_YELL_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_DRIP_ID, SOUND_DRIP_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_AMONG_US_HIT_ID, SOUND_AMONG_US_HIT_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_AMONG_US_KILL_ID, SOUND_AMONG_US_KILL_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_REPORT_ID, SOUND_REPORT_EVENT);
        Registry.register(Registry.SOUND_EVENT, SOUND_AMONG_US_WALK_ID, SOUND_AMONG_US_WALK_EVENT);

        Amogus.LOGGER.info("Sounds registered");
    }
}
