package sus.deadmeme.amogus.registry;

import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.fabricmc.fabric.api.event.client.ClientSpriteRegistryCallback;
import net.fabricmc.fabric.api.particle.v1.FabricParticleTypes;
import net.minecraft.client.particle.FlameParticle;
import net.minecraft.particle.DefaultParticleType;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.effect.SusCooldownEffect;
import sus.deadmeme.amogus.effect.SusPowerEffect;

public class AmogusMisc {
    // Effects proper
    public static final SusPowerEffect SUS_POWER = new SusPowerEffect();
    public static final SusCooldownEffect SUS_COOLDOWN = new SusCooldownEffect();

    // Effects identifiers
    public static final Identifier SUS_POWER_ID = new Identifier(Amogus.MOD_ID, "sus_power");
    public static final Identifier SUS_COOLDOWN_ID = new Identifier(Amogus.MOD_ID, "sus_cooldown");

    // Particles proper
    public static final DefaultParticleType SUS_PARTICLE = FabricParticleTypes.simple();

    // Particle identifiers
    public static final Identifier SUS_PARTICLE_ID = new Identifier(Amogus.MOD_ID, "sus_particle");

    public static void init() {
        Registry.register(Registry.STATUS_EFFECT, SUS_POWER_ID, SUS_POWER);
        Registry.register(Registry.STATUS_EFFECT, SUS_COOLDOWN_ID, SUS_COOLDOWN);

        Amogus.LOGGER.info("Status effects registered");

        Registry.register(Registry.PARTICLE_TYPE, SUS_PARTICLE_ID, SUS_PARTICLE);

        Amogus.LOGGER.info("Particles registered on server");
    }

    public static void clientInit() {
        // Add particle texture to texture atlas
        ClientSpriteRegistryCallback.event(PlayerScreenHandler.BLOCK_ATLAS_TEXTURE).register((atlasTexture, registry) ->{
            registry.register(new Identifier(Amogus.MOD_ID, "particle/sus_particle"));
        });

        // Client side particle registering
        ParticleFactoryRegistry.getInstance().register(AmogusMisc.SUS_PARTICLE, FlameParticle.Factory::new);
    }
}
