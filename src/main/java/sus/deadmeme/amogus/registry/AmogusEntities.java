package sus.deadmeme.amogus.registry;

import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.entity.ImpostorEntity;
import sus.deadmeme.amogus.gecko.renderer.ImpostorEntityRenderer;

public class AmogusEntities {

    // TODO change spawn group later and change natural spawning to per-biome
    public static EntityType<ImpostorEntity> IMPOSTOR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(Amogus.MOD_ID, "impostor"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ImpostorEntity::new)
                    .dimensions(EntityDimensions.fixed(0.75f, 0.75f))
                    .build()
    );

    public static void init() {
        FabricDefaultAttributeRegistry.register(IMPOSTOR, ImpostorEntity.createImpostorAttributes());
    }

    public static void clientInit() {
        EntityRendererRegistry.register(IMPOSTOR, ImpostorEntityRenderer::new);
    }
}
