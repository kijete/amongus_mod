package sus.deadmeme.amogus.registry;

import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.MapColor;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.item.BlockItem;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.block.PonderOrb;
import sus.deadmeme.amogus.block.entity.PonderOrbEntity;
import sus.deadmeme.amogus.gecko.renderer.PonderOrbRenderer;

public class AmogusBlocks {

    public static final Identifier PONDER_ORB_ID = new Identifier(Amogus.MOD_ID, "ponder_orb");
    public static final PonderOrb PONDER_ORB = new PonderOrb(FabricBlockSettings
            .of(Material.AMETHYST, MapColor.PALE_PURPLE)
            .requiresTool()
            .strength(5.0f, 6.0f)
            .sounds(BlockSoundGroup.AMETHYST_BLOCK)
            .luminance(8)
            .nonOpaque());
    public static BlockEntityType<PonderOrbEntity> PONDER_ORB_ENTITY;

    public static void init() {

        Registry.register(Registry.BLOCK, PONDER_ORB_ID, PONDER_ORB);

        Registry.register(Registry.ITEM, PONDER_ORB_ID, new BlockItem(PONDER_ORB,
                new FabricItemSettings().group(Amogus.AMONG_GROUP).rarity(Rarity.EPIC)));

        PONDER_ORB_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, Amogus.MOD_ID + ":ponder_orb_entity",
                FabricBlockEntityTypeBuilder.create(PonderOrbEntity::new, PONDER_ORB).build());

        Amogus.LOGGER.info("Blocks registered");
    }

    public static void clientInit() {

        BlockEntityRendererRegistry.register(AmogusBlocks.PONDER_ORB_ENTITY, (BlockEntityRendererFactory.Context rendererDispatcher) ->
            new PonderOrbRenderer()
        );

    }
}
