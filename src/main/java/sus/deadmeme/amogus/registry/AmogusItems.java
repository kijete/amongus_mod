package sus.deadmeme.amogus.registry;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;
import org.lwjgl.system.CallbackI;
import software.bernie.geckolib3.renderers.geo.GeoItemRenderer;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.gecko.renderer.PlushieItemRenderer;
import sus.deadmeme.amogus.item.AmongItem;
import sus.deadmeme.amogus.item.Plushie;
import sus.deadmeme.amogus.item.Tag;

// This class creates, and through init(), registers the items in the mod.
public class AmogusItems {

    public static final AmongItem AMONG_ITEM = new AmongItem(new FabricItemSettings()
            .group(Amogus.AMONG_GROUP)
            .food(new FoodComponent.Builder()
                    .alwaysEdible()
                    .hunger(-3)
                    .build()));
    public static final Item IMPOSTOR_SPAWN_EGG = new SpawnEggItem(
            AmogusEntities.IMPOSTOR,
            0xcb1400,
            0x0058cb,
            new FabricItemSettings().group(Amogus.AMONG_GROUP));
    public static final Plushie PLUSHIE = new Plushie(new FabricItemSettings()
            .group(Amogus.AMONG_GROUP)
            .maxCount(1)
            .rarity(Rarity.EPIC)
            .fireproof());
    public static final Tag TAG = new Tag(new FabricItemSettings()
            .group(Amogus.AMONG_GROUP)
            .maxCount(1)
            .rarity(Rarity.RARE)
            .recipeRemainder(AmogusItems.TAG)
            .fireproof());

    public static void init() {
        Registry.register(Registry.ITEM, new Identifier(Amogus.MOD_ID, "among_item"), AMONG_ITEM);
        Registry.register(Registry.ITEM, new Identifier(Amogus.MOD_ID, "impostor_spawn_egg"), IMPOSTOR_SPAWN_EGG);
        Registry.register(Registry.ITEM, new Identifier(Amogus.MOD_ID, "plushie"), PLUSHIE);
        Registry.register(Registry.ITEM, new Identifier(Amogus.MOD_ID, "tag"), TAG);

        Amogus.LOGGER.info("Items registered");
    }

    public static void clientInit() {
        GeoItemRenderer.registerItemRenderer(PLUSHIE, new PlushieItemRenderer());
    }
}
