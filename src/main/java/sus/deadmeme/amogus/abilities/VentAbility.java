package sus.deadmeme.amogus.abilities;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import sus.deadmeme.amogus.Amogus;
import sus.deadmeme.amogus.registry.AmogusMisc;
import sus.deadmeme.amogus.utils.AmogusNetwork;

import java.util.Iterator;
import java.util.Random;

public class VentAbility {

    // Calculates the teleport position for venting.
    public static BlockPos calculateVentPos(double yaw, double pitch, PlayerEntity player, World world) {
        // Get head position.
        BlockPos headPos = player.getBlockPos().up(1);

        // Convert the yaw and pitch into vector.
        Vec3d facing = Vec3d.fromPolar((float) pitch, (float) yaw).normalize();

        BlockPos facingIncrement = new BlockPos(Math.round(facing.getX()), Math.round(facing.getY()), Math.round(facing.getZ()));

        BlockPos block = headPos;
        BlockPos telepos = block;
        boolean seenWall = false;
        // Iterate forward 10 times over the player's ray of sight...
        for (int distance = 0; distance < 10; distance++) {
            // Computing on every block in that ray...
            block = block.add(facingIncrement);
            BlockState blockAtPos = world.getBlockState(block);
            // And the block below it, for safety...
            BlockState blockBelow = world.getBlockState(block.down(1));

            if (blockAtPos.isAir() && blockBelow.isAir()) {
                // If area is safe, set the telepos to food position...
                telepos = block.down(1);

                // If this is the first safe area after we see a wall, break the loop. We will teleport there.
                if (seenWall) break;

                // If we are already 5 blocks away and there are no walls, stop.
                if (distance >= 5) break;

            } else {
                // If not, we have seen a wall!
                seenWall = true;
            }
        }

        // Now that we have a safe teleport space, check to make sure it isn't in the air.
        while (true) {
            if (!world.getBlockState(telepos.down(1)).isAir()) break;
            telepos = telepos.down(1);
        }

        // Now return the telepos.
        return telepos;
    }

    // SERVER ONLY
    // Receives vent packet, calculates vent, and teleports if not suspicious (haha).
    public static void receiveVent(MinecraftServer server,
                                   ServerPlayerEntity player,
                                   ServerPlayNetworkHandler handler,
                                   PacketByteBuf buf,
                                   PacketSender responseSender) {

        // Read the requested pos and camera rotation.
        BlockPos requestedPos = buf.readBlockPos();
        double yaw = buf.readDouble();
        double pitch = buf.readDouble();

        // Calculate telepos for later comparison.
        BlockPos telepos = VentAbility.calculateVentPos(yaw, pitch, player, player.world);


        // If they are close, teleport the player.
        if (requestedPos.isWithinDistance(telepos, 5)) {
            player.setVelocity(0, 0, 0);
            player.requestTeleport(requestedPos.getX(), requestedPos.getY(), requestedPos.getZ());
            player.addStatusEffect(new StatusEffectInstance(AmogusMisc.SUS_COOLDOWN, 20));
            player.world.playSound(null, requestedPos,
                    SoundEvents.ENTITY_ENDERMAN_TELEPORT,
                    SoundCategory.PLAYERS, 1f, 1f);
        }
    }

    // CLIENT ONLY
    // Send the packet to the server telling it the player has vented.
    // Only here to keep the garbage code in one place.
    public static void sendVentPacket(MinecraftClient client) {
        // Check if the player has SUS_POWER. If they don't, why bother?
        Iterator<StatusEffectInstance> statusEffects = client.player.getStatusEffects().iterator();
        boolean hasPower = false;
        // Does player have SUS_POWER?
        while (statusEffects.hasNext()) {
            hasPower = (statusEffects.next().getEffectType() == AmogusMisc.SUS_POWER);
            if (hasPower) break;
        }
        if (!hasPower) return;

        // Get camera rotation.
        double yaw = client.cameraEntity.getYaw();
        double pitch = client.cameraEntity.getPitch();

        // Create a buffer for the packet.
        PacketByteBuf buffer = PacketByteBufs.create();

        // Write the BlockPos of the requested teleport location.
        // If there is a large discrepancy (The server calculates it separately) it will not teleport.
        BlockPos ventPos = VentAbility.calculateVentPos(yaw, pitch, client.player, client.player.world);
        buffer.writeBlockPos(ventPos);

        // Write rotation individually to byte buffer.
        // REMEMBER TO READ IN SAME YAW-PITCH ORDER
        buffer.writeDouble(yaw);
        buffer.writeDouble(pitch);



        // Send the packet to the server!
        ClientPlayNetworking.send(AmogusNetwork.VENT_PACKET_ID, buffer);

        // Add puff of sus.
        Random random = new Random();
        for (int loop = 0; loop < 200; loop++) {
            client.world.addParticle(AmogusMisc.SUS_PARTICLE,
                    ventPos.up(2).getX(),
                    ventPos.up(2).getY(),
                    ventPos.up(2).getZ(),
                    0.05 * (random.nextDouble(2) - 1),
                    0.05 * (random.nextDouble(2) - 1),
                    0.05 * (random.nextDouble(2) - 1));
        }
        for (int loop = 0; loop < 500; loop++) {
            client.world.addParticle(AmogusMisc.SUS_PARTICLE,
                    ventPos.up(2).getX(),
                    ventPos.up(2).getY(),
                    ventPos.up(2).getZ(),
                    1 * (random.nextDouble(2) - 1),
                    1 * (random.nextDouble(2) - 1),
                    1 * (random.nextDouble(2) - 1));
        }
    }
}