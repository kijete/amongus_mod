package sus.deadmeme.amogus.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.util.registry.Registry;

public class SusPowerEffect extends StatusEffect {
    public SusPowerEffect() {
        super(StatusEffectCategory.BENEFICIAL, 0xe2e0d9);
    }
    // Does literally nothing
}