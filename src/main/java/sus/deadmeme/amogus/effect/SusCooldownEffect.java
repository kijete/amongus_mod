package sus.deadmeme.amogus.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;

public class SusCooldownEffect extends StatusEffect {
    public SusCooldownEffect() {
        super(StatusEffectCategory.HARMFUL, 0xd36256);
    }
    // Does literally nothing. Just used to keep a cooldown.
}